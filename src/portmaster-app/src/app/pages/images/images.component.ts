import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pm-images',
  templateUrl: 'images.component.html'
})
export class ImagesComponent implements OnInit {

  constructor(
    private http: HttpClient
  ) { }

  images: string[] = [];
  showingImg: string = '';

  ngOnInit(): void {
    this.http.get('/registry-api/v2/_catalog?n=10')
      .subscribe({
        next: (res: any) => {
          console.debug(res);
          this.images = res.repositories;
        },
        error: (error) => {
          console.log(error);
        }
      });
  }

}
