import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pm-root',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {

  constructor(
    private http: HttpClient
  ) { }

  isApiV2: boolean = false;
  ngOnInit(): void {
    this.http.get('/registry-api/v2/_catalog?n=1')
      .subscribe({
        next: () => {
          this.isApiV2 = true;
        },
        error: (error) => {
          console.log(error);
        }
      });
  }
}