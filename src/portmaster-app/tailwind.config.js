module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        theme: {
          blue: "#2496ed",
          "blue-dark": "#058ef7",
          "blue-darker": "#0277d1"
        }
      }
    },
  },
  plugins: [],
}
